var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var url = require('../config/keys').MongoURI;
var { ensureAuthenticated } = require('../config/auth');
router.get('/', (req, res) => {
    res.render('home-page');
});

router.get('/dashboard', (req, res) => {
    res.render('dashboard', {
        name: req.user.name,
        phone: req.user.phone,
        email: req.user.email,
        friends: req.user.friends
    });
});



module.exports = router;