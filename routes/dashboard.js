var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var url = require('../config/keys').MongoURI;
var async = require('async');
var router = express.Router();
var login = require("facebook-chat-api");
var twilio = require('twilio');
var nodemailer = require('nodemailer');

var User = require('../models/user');
var buddies;

const accountSid = 'ACdb613bd1c91b8405571a1563a6fdfc24'
const authToken = '00e954fe556e835866a5f5b76c26a2fe';
const client = require('twilio')(accountSid, authToken);

router.get('/add-friend', (req, res) => {
    res.render('add-friend', {
        friends: req.user.friends
    });
});

router.post('/add-friend', (req, res) => {
    buddies = []
    MongoClient.connect(url, (err, db) => {
        var dbo = db.db('test');
        dbo.collection('users').find({ name: { $regex: new RegExp('^' + req.body.name, 'i') } }).toArray((err, friends) => {
            if (err) throw err
            var trueFriends = []
            for (var i = 0; i < friends.length; i++) {
                if (friends[i].name != req.user.name) {
                    trueFriends.push(friends[i])
                    buddies.push(friends[i])
                }
            }
            res.render('add-friend', {
                trueFriends
            }
            );
        })
    })
})

router.post('/add-new-friend', (req, res) => {
    console.log('body: ' + JSON.stringify(req.body));
    var currFriends = req.user.friends;
    MongoClient.connect(url, (err, db) => {
        var dbo = db.db('test');
        dbo.collection('users').findOne({ email: req.body.friendEmail }, (err, result) => {
            if (err) throw err
            if (!currFriends.includes(result)) {
                currFriends.push(result)
                dbo.collection('users').update({ email: req.user.email }, { $set: { friends: currFriends } }, (err, result) => {
                    if (err) throw err
                })
            }
        })

        dbo.collection('users').findOne({ email: req.body.friendEmail }, (err, result) => {
            if (err) throw err
            var friendArr = result.friends;
            if (!friendArr.includes(req.user)) {
                friendArr.push(req.user);
                dbo.collection('users').update({ email: req.body.friendEmail }, { $set: { friends: friendArr } }, (err, result) => {
                    if (err) throw err
                })
            }
        })
    })
})

router.post('/wuphf', (req, res) => {
    var index = req.body.index
    do_it(accountSid, authToken, client, req.user.friends[1].phone, req.user.friends[1].name, req.user.friends[1].email)
    res.redirect('/dashboard')
})

function do_it(accountSid, authToken, client, phone, name, email) {
    client.calls
        .create({
            url: `http://twimlets.com/message?Message%5B0%5D=Woof%20Woof%20Woof%20Woof%20Woof%20Woof%20Woof%20Woof%20Woof%20Woof&`,
            to: '+14086218538',//reciever phone number
            from: '+14082127661'
        })
        .then(call => console.log("called", call));

    client.messages
        .create({
            from: '+14082127661',
            body: 'You\'re getting a wuphf',
            to: '+14086218538'
        })
        .then(message => console.log("messaged"));


    client.messages
        .create({
            from: 'whatsapp:+14155238886',
            body: 'Hey there buddy, you just got a wuphf',
            to: 'whatsapp:+14086218538'
        })
        .then(message => console.log(message.sid));



    login({ email: "wuphf.net@gmail.com", password: "vQqUdtnAXt3fV4B" }, (err, api) => {
        if (err) return console.error(err);
        api.getUserID(name, (err, data) => {
            if (err) return console.error(err);

            var msg = "Wuphf Wuphf Wuphf, you just got a wuphf"
            var threadID = data[0].userID;
            api.sendMessage(msg, threadID);
        });
    });

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'wuphf.net@gmail.com',
            pass: 'I79GLgu0P6xU'
        }
    });

    var mailOptions = {
        from: 'wuphf.net@gmail.com',
        to: email,
        subject: 'You have received a wuphf',
        text: 'Someone is trying to reach you on wuphf, it is probably important and you should check all your chats.'
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}




module.exports = router;